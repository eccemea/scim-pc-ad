let pc = require('./purecloud');
const defer = require('promise-defer');
let ActiveDirectory = require('activedirectory');

var config = {
    url: process.env.AD_URL,
    baseDN: process.env.AD_BASE_DN,
    username: process.env.AD_USERNAME,
    password: process.env.AD_PASSWORD

}


let adUserKey = process.env.AD_USER_KEY // key to match PureCloud user with AD User
let DIVISION_DN = process.env.DIVISION_DN;
let GROUPS_DN = process.env.GROUPS_DN;

var ad = new ActiveDirectory(config);


function login() {
    console.log(`login() to the AD url: ${config.url}`);
    return new Promise(function (resolve, reject) {

        ad.authenticate(config.username, config.password, function (err, auth) {
            if (err) {
                console.log('ERROR: ' + JSON.stringify(err));
                reject();
                return;
            }

            if (auth) {
                console.log('AD authenticated!');
                // Login to PureCloud

                pc.login().then(function (resp) {
                    resolve(auth);

                }).catch(function (err) {

                })

            }
            else {
                console.log('Authentication failed!');
                reject();
            }
        });
    });

}

function getDivision(_groups) {
    if (DIVISION_DN.length > 0) {
        console.log('getDivision from ad...');
        for (var x = 0; x < _groups.length; x++) {
            if (_groups[x].dn.indexOf(DIVISION_DN) !== -1) {
                console.log(`found division: ${_groups[x].cn}`);
                return _groups[x].cn
                break
            }
        }
    }
    return "Home" //default

}


function getGroups(_groups) {
    if (GROUPS_DN.length > 0) {
        var myGroups = []
        console.log('find groups');
        for (var x = 0; x < _groups.length; x++) {
            if (_groups[x].dn.indexOf(GROUPS_DN) !== -1) {
                console.log(`add group to assign: ${_groups[x].cn}`);
                myGroups.push(_groups[x].cn);
            }
        }
        console.log(myGroups);
        return myGroups
    }

}


function sync() {
    console.log('sync() ad objects');
    return new Promise(function (resolve, reject) {
        var opts = {
            attributes: ["*"],
            filter: 'cn=*',
            includeMembership: ['group', 'user'],
            includeDeleted: true
        };

        ad.find(opts, function (err, results) {
            if ((err) || (!results)) {
                console.log('ERROR: ' + JSON.stringify(err));
                reject();
                return;
            }

            console.log('scan ad users...');
            checkUsers(results.users, 0).then(function (resp) {
                console.log('all users scanned');
                console.log(resp);
                resolve();

                /*
                console.log('Groups');
                _.each(results.groups, function (group) {
                    console.log(group);
                });
                */
            }).catch(function (err) {
                console.log('catch');
                console.log(err);
                reject();
            })

        });
    });
}

function isAccountEnabled(_value) {
    // userAccountControl: https://social.technet.microsoft.com/Forums/en-US/69211f96-b17e-43aa-9a6a-4f8e99ae2b3a/useraccountcontrol-and-employeestatus?forum=ilm2
    /*
        512	Enabled Account
        514	Disabled Account
        544	Enabled, Password Not Required
        546	Disabled, Password Not Required
        66048	Enabled, Password Doesn't Expire
        66050	Disabled, Password Doesn't Expire
        66080	Enabled, Password Doesn't Expire & Not Required
        66082	Disabled, Password Doesn't Expire & Not Required
        262656	Enabled, Smartcard Required
        262658	Disabled, Smartcard Required
        262688	Enabled, Smartcard Required, Password Not Required
        262690	Disabled, Smartcard Required, Password Not Required
        328192	Enabled, Smartcard Required, Password Doesn't Expire
        328194	Disabled, Smartcard Required, Password Doesn't Expire
        328224	Enabled, Smartcard Required, Password Doesn't Expire & Not Required
        328226	Disabled, Smartcard Required, Password Doesn't Expire & Not Required
    */

    if (_value == '512' || _value == '544' || _value == '66048' || _value == '66080' || _value == '262656' || _value == '262688' || _value == '328192' || _value == '328224') {
        // Account Enabled
        return true

    } else {
        // Account Disabled
        return false
    }
}

function checkUsers(_adUsers, _userIndex, _counters, _def) {

    if (!_def) {
        _counters = {
            adValidUsers: 0,
            pcInSync: 0,
            adSkippedUsers: 0,
            pcUpdated: 0,
            pcCreated: 0,
            pcUpdateFailed: 0,
            pcCreateFailed: 0
            
        }
    }
    var deferred = _def || new defer();

    let user = _adUsers[_userIndex];
    //console.log(user);
    if (user) {
        console.log(`---------\nad user: ${user.cn}`);
        try {
            /*
            if (user.isDeleted) {
                // Delte user from PureCloud
                console.log(user);
                
               // checkUsers(_adUsers, _userIndex + 1, _counters, deferred);

            } else */if (!user[adUserKey]) {

                console.log(`adKey object (${adUserKey}) not present, continue with next ad user`);
                _counters.adSkippedUsers++;
                checkUsers(_adUsers, _userIndex + 1, _counters, deferred);

            } else {
                // User Found in AD, get more data
                _counters.adValidUsers++;
                let adDivision = getDivision(user.groups);
                let adAccountEnabled = isAccountEnabled(user.userAccountControl);
                //let myGroups = getGroups(user.groups);
                console.log(`ad User's details -> isEnabled: ${adAccountEnabled}, division: ${adDivision}, displayName: ${user.displayName}, key(${adUserKey}): ${user[adUserKey]}`);

                pc.findUser(user[adUserKey]).then(function (pcUser) {
                    if (pcUser.totalResults == 1) {
                        // userExits in PC, verify active / division / displayName
                        console.log('purecloud: user already defined in PureCloud');

                        let pcDivision = pcUser.Resources[0]['urn:ietf:params:scim:schemas:extension:enterprise:2.0:User'].division;
                        let pcActive = pcUser.Resources[0].active;
                        let pcDisplayName = pcUser.Resources[0].displayName;

                        console.log(`pc User\'s details -> isEnabled: ${pcActive}, division: ${pcDivision}, displayName: ${pcUser.Resources[0].displayName}`);

                        if (pcDivision != adDivision || pcDisplayName != user.displayName || pcActive != adAccountEnabled) {
                            // Something is different / update.
                            console.log('user details are different, update...');
                            pc.updateUser(pcUser.Resources[0].id, user.displayName, adDivision, adAccountEnabled).then(function () {
                                _counters.pcUpdated++;
                                checkUsers(_adUsers, _userIndex + 1, _counters, deferred);

                            }).catch(function () {
                                _counters.pcUpdateFailed++;
                                checkUsers(_adUsers, _userIndex + 1, _counters, deferred);
                            })

                        } else {
                            // division match, nothing to update
                            console.log('users in sync.');
                            _counters.pcInSync++;
                            checkUsers(_adUsers, _userIndex + 1, _counters, deferred);
                        }

                    } else {
                        console.log('user not found in PureCloud');
                        // user not found in PC, let's create him
                        pc.createUser(user[adUserKey], user.displayName, adDivision).then(function () {
                            _counters.pcCreated++;
                            checkUsers(_adUsers, _userIndex + 1, _counters, deferred);
                        }).catch(function () {
                            _counters.pcCreateFailed++;
                            checkUsers(_adUsers, _userIndex + 1, _counters, deferred);
                        })

                    }

                }).catch(function () {
                    deferred.reject();
                })
            }
        } catch (error) {
            console.log(error);
            deferred.reject();
        }

    } else {
        // end loop. All users processed
        deferred.resolve(_counters);
    }

    return deferred.promise;
}

module.exports = {
    login,
    sync
};
