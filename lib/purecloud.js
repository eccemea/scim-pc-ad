let platformClient = require('purecloud-platform-client-v2');


let client = platformClient.ApiClient.instance;
let apiInstance = new platformClient.SCIMApi();

let clientId = process.env["CLIENT_ID"];
let clientSecret = process.env["CLIENT_SECRET"];
let env = process.env["ENV"];


// set PureCloud environment
client.setEnvironment(env);

// Login to the PureCloud and retreive Users and Queues
function login() {
    console.log('login PureCloud()');
    console.log('PureCloud auth ---');
    console.log(`env         : ${env}`);
    console.log(`clientId    : ${clientId}`);
    console.log(`clientSecret: ${clientSecret.substring(0, 10)}(...)`);

    return new Promise(function (resolve, reject) {
        try {

            client.loginClientCredentialsGrant(clientId, clientSecret)
                .then(function () {
                    console.log('service is connected to PureCloud!');

                    let resp = {
                        token: client.authData.accessToken,
                        expiryTime: client.authData.tokenExpiryTime
                    }
                    client.setAccessToken(resp.token);
                    resolve(resp);

                })
                .catch(function (err) {
                    console.log(err);
                    reject();
                });

        } catch (err) {
            console.log(err);
            reject();
        }
    });
}

function findUser(_userName) {
    console.log(`purecloud: findUser`);
    return new Promise(function (resolve, reject) {
        try {

            let filter = `userName eq ${_userName}`; // String | Filters results.
            let opts = {
                'startIndex': 1, // Number | The 1-based index of the first query result.
                'count': 0 // Number | The requested number of items per page. A value of 0 returns totalResults.
            };

            apiInstance.getScimUsers(filter)
                .then((data) => {
                    resolve(data);

                })
                .catch((err) => {
                    if (err.status == 429) {
                        console.log('too many requests - sleep for a while then repeat last interval/page...');
                        setTimeout(function () {
                            findUser(_userName).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                console.log('there was a failure calling getScimUsers');
                                console.log(err);
                                reject(err);
                            })
                        }, 30000);
                    } else {
                        console.log('there was a failure calling getScimUsers');
                        console.log(err);
                        reject(err);
                    }
                });

        } catch (err) {
            console.log(err);
            reject();

        }
    });
};

function createUser(_userName, _displayName, _division, _active) {
    console.log(`purecloud createUser (${_userName})`);
    return new Promise(function (resolve, reject) {
        try {

            let body = {
                "userName": _userName,
                "displayName": _displayName,
                "divion": _division,
                "active": _active
            }

            apiInstance.postScimUsers(body)
                .then((data) => {
                    console.log('user created.');
                    resolve(data);

                })
                .catch((err) => {
                    if (err.status == 429) {
                        console.log('too many requests - sleep for a while then repeat last interval/page...');
                        setTimeout(function () {
                            createUser(_userName, _displayName, _division, _active).then(function (data) {
                                console.log('user created.');
                                resolve(data);
                            }).catch(function (err) {
                                console.log('there was a failure calling postScimUsers');
                                console.log(err);
                                reject(err);
                            })
                        }, 30000);
                    } else {
                        console.log('there was a failure calling postScimUsers');
                        console.log(err);
                        reject(err);
                    }
                });

        } catch (err) {
            console.log(err);
            reject();

        }
    });
};

function updateUser(_userId, _displayName, _division, _active) {
    console.log(`purecloud updateUser (${_userId})`);
    return new Promise(function (resolve, reject) {
        try {

            let body = {
                "active": _active,
                "displayName": _displayName,
                "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User": { "division": _division }
            }
            console.log(body);
            apiInstance.putScimUser(_userId, body)
                .then((data) => {                    
                    resolve(data);
                })
                .catch((err) => {
                    if (err.status == 429) {
                        console.log('too many requests - sleep for a while then repeat last interval/page...');
                        setTimeout(function () {
                            updateUser(_userId, _displayName, _division, _active).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                console.log('there was a failure calling putScimUser');
                                console.log(err);
                                reject(err);
                            })
                        }, 30000);
                    } else {
                        console.log('there was a failure calling putScimUser');
                        console.log(err);
                        reject(err);
                    }
                });

        } catch (err) {
            console.log(err);
            reject();

        }
    });
};

// Not used
function deleteUser(_userId) {
    console.log(`purecloud deleteUser (${_userId})`);
    return new Promise(function (resolve, reject) {
        try {
 
            apiInstance.deleteScimUser(_userId)
                .then((data) => {                    
                    resolve(data);
                })
                .catch((err) => {
                    if (err.status == 429) {
                        console.log('too many requests - sleep for a while...');
                        setTimeout(function () {
                            deleteUser(_userId).then(function (data) {
                                resolve(data);
                            }).catch(function (err) {
                                console.log('there was a failure calling deleteScimUser');
                                console.log(err);
                                reject(err);
                            })
                        }, 30000);
                    } else {
                        console.log('there was a failure calling deleteScimUser');
                        console.log(err);
                        reject(err);
                    }
                });

        } catch (err) {
            console.log(err);
            reject();

        }
    });
};



module.exports = {
    login,
    findUser,
    createUser,
    updateUser,
    deleteUser

};
