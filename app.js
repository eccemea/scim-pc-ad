//TODO: dynamic version from package.json
require('dotenv').config();

let json = require('./package.json')
let ad = require('./lib/ad');

let express = require('express');


let app = express();
let port = process.env.PORT || 3000;
var bServerDuringSync = false;


ad.login().then(function () {
    console.log('Sync every 2min');
    setInterval(function () {
        if (!bServerDuringSync) {
            bServerDuringSync = true;
            ad.sync().then(function () {
                bServerDuringSync = false;
            }).catch(function () {
                bServerDuringSync = false;
            })
        } 

    },  2 * 60 * 1000); // sync every 2 min

}).catch(function () {

})



// Welcome message for browser
app.get('/', function (req, res) {

    res.writeHead(200, {
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    });

    res.end('{\'response\': \'Service up & running v.' + json.version + '\'}');
})


console.log(`service started on port: ${port}`);
app.listen(port);
