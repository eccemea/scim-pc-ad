# SCIM proof-of-concept Sync MS AD on Prem with PureCloud
This service is early version of sync on Prem AD with PureCloud, where AD is a master.

Supported functionality:
* Create new user in PureCloud
* Update user in PureCloud (DisplayName, isActive, division)
* 429 is supported with 30sec retry

`` DELETE Objects in PureCloud are not supported ``

## Custom Mapping between AD and PureCloud
By default main key that is used to match users between PureCloud and AD is
``AD_USER_KEY = "userPrincipalName"``

This can be changed to other fields returned from AD, for example ``mail`` 

## Divisions
By default, even if division is not set, service will use ``Home`` division.
To support custom divisions, create new Container in AD and assigned it in mappings. Only first assigned Division object will be used.

For example:

In AD you create new Object called ``Divisions`` 
![picture](img/divisions.png)


 with names "Cupertino" , "Home"

For selected user assignh him to the newly created division object (Cupertino):
![picture](img/division_for_user.png)

Because you use for divisions OU called "Divisions" make a proper config in service .env file:

`` DIVISION_DN = "OU=Divisions,DC=home,DC=pl" ``


## Example of.env file:
.env file need to be created manually on Prem.

Below sample configuration:

```
CLIENT_ID = "a3f"
CLIENT_SECRET = "SJ3oDum"
ENV = "mypurecloud.ie"

AD_URL = "ldap://10.211.55.6"
AD_BASE_DN = "dc=home,dc=pl"
AD_USERNAME = "administrator@home.pl"
AD_PASSWORD = "Y0ur5ecretP@ssw0rd"

AD_USER_KEY = "userPrincipalName"
DIVISION_DN = "OU=Divisions,DC=home,DC=pl"

```

## Installation
On Windows install https://nodejs.org/en/, then copy source of this repo to the folder and run:

`` npm install ``

when finish, start service:

`` node . ``

## Todo
*   Install serivce as a Windows Service
*   Add support for Groups
*   Support for DELETED Objects from AD?

